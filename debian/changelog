python-fitbit (0.3.1-5) unstable; urgency=medium

  * Team upload.
  * Bump standards version to 4.7.0 (no changes needed)
  * Remove unnecessary setuptools runtime dependency (Closes: #1083637)

 -- Ananthu C V <weepingclown@disroot.org>  Tue, 03 Dec 2024 23:06:12 +0530

python-fitbit (0.3.1-4) UNRELEASED; urgency=medium

  * Team upload.
  * UNRELEASED

 -- Ananthu C V <weepingclown@disroot.org>  Tue, 03 Dec 2024 22:57:08 +0530

python-fitbit (0.3.1-3) unstable; urgency=medium

  [ Étienne Mollier ]
  * Team upload.
  * d/control: remove dependency to nose, since tests are not run.
    (Closes: #1018492)
  * Standards-Version: 4.6.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * d/copyright: bump year for debian/*.

 -- Ananthu C V <weepingclown@disroot.org>  Tue, 03 Dec 2024 22:56:56 +0530

python-fitbit (0.3.1-2) unstable; urgency=medium

  * Team upload.
  * Drop Python2 support
    Closes: #937755
  * debhelper-compat 12
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target

 -- Andreas Tille <tille@debian.org>  Wed, 04 Sep 2019 13:25:02 +0200

python-fitbit (0.3.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version: 4.4.0 (no changes needed).

 -- Dylan Aïssi <daissi@debian.org>  Fri, 12 Jul 2019 20:30:59 +0200

python-fitbit (0.3.0-4) unstable; urgency=medium

  * Add Testsuite: autopkgtest-pkg-python
  * Bump Standards-Version: 4.2.1.
  * Fix VCS fields (salsa migration).
  * Mark python-fitbit-doc as "Multi-Arch: foreign".

 -- Dylan Aïssi <bob.dybian@gmail.com>  Sun, 16 Sep 2018 10:45:29 +0200

python-fitbit (0.3.0-3) unstable; urgency=medium

  * Add python-fitbit-doc: missing Breaks+Replaces: python-fitbit (<< 0.3.0-2)
     (Closes: #891024).

 -- Dylan Aïssi <bob.dybian@gmail.com>  Wed, 21 Feb 2018 21:36:43 +0100

python-fitbit (0.3.0-2) unstable; urgency=medium

  * Move documentation into a new package python-fitbit-doc.
  * Bump Standards-Version: 4.1.3 (no changes needed).
  * Update debhelper compat to 11.
  * Build new package python3-fitbit (Python 3).
  * Remove Build-Depends required by disabled tests.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Fri, 16 Feb 2018 22:55:38 +0100

python-fitbit (0.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Update d/copyright 2017.
  * Update Build-Depends:
   - python-requests-oauthlib (>= 0.7)
   - python-freezegun (>= 0.3.8)
   - python-requests-mock (>= 1.2.0)
  * debian/{changelog,rules}: Remove trailing whitespace characters.
  * Bump Standards-Version: 4.1.1 (no changes needed).

 -- Dylan Aïssi <bob.dybian@gmail.com>  Sat, 11 Nov 2017 23:48:45 +0100

python-fitbit (0.2.4-1) unstable; urgency=medium

  * New upstream release.
  * Update Uploaders: Add myself and remove previous ones.
  * Bump Standards-Version to 3.9.8.
  * Update d/copyright.
  * Remove obsolete X-Python-Version field.
  * Update watch file to v4.
  * Update debhelper compat to 10.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Fri, 09 Dec 2016 22:38:24 +0100

python-fitbit (0.2.3-1) unstable; urgency=medium

  * Imported new upstream version 0.2.3
  * debian/control:
   - Added versioned dependency on python-requests-oauthlib (Closes: #823842)

 -- Iain R. Learmonth <irl@debian.org>  Sun, 18 Sep 2016 12:35:48 +0100

python-fitbit (0.2-1) unstable; urgency=medium

  * Imported Upstream version 0.2
  * Tests are disabled during build, they require network access.
  * debian/control:
   - Standards version updated to 3.9.7
   - Changed my email address to irl@debian.org
   - Removed versioned dependency for python-sphinx
   - Vcs-* fields changed to secure URIs

 -- Iain R. Learmonth <irl@debian.org>  Mon, 04 Apr 2016 19:17:51 +0100

python-fitbit (0.1.3-2) unstable; urgency=medium

  * Replace tox by nose to avoid creating a virtualenv and downloading
    remote data (Closes: #796538).

 -- Olivier Sallou <osallou@debian.org>  Sat, 22 Aug 2015 13:17:43 +0000

python-fitbit (0.1.3-1) unstable; urgency=medium

  * Updated for upstream release 0.1.3.

 -- Iain R. Learmonth <irl@fsfe.org>  Sun, 08 Feb 2015 00:46:51 +0000

python-fitbit (0.1.2-2) unstable; urgency=medium

  * Delete caches from Sphinx documentation allowing for reproducible builds.

 -- Iain R. Learmonth <irl@fsfe.org>  Wed, 04 Feb 2015 23:41:14 +0000

python-fitbit (0.1.2-1) unstable; urgency=medium

  * Updated for upstream release 0.1.2.
  * Update to Debian Standards Version 3.9.6.
  * Update VCS browser URL for new VCS browser.

 -- Iain R. Learmonth <irl@fsfe.org>  Fri, 26 Sep 2014 20:01:09 +0100

python-fitbit (0.1.0-1) unstable; urgency=medium

  * Updated for upstream release 0.1.0.

 -- Iain R. Learmonth <irl@fsfe.org>  Tue, 03 Jun 2014 18:48:35 +0100

python-fitbit (0.0.3-2) unstable; urgency=medium

  * Updated for upstream release 0.0.3.
  * Corrected doc-base Document field (Closes: #739107)

 -- Iain R. Learmonth <irl@fsfe.org>  Sat, 15 Feb 2014 21:14:13 +0000

python-fitbit (0.0.2-1) unstable; urgency=medium

  * Initial release. (Closes: #737566)

 -- Iain R. Learmonth <irl@fsfe.org>  Mon, 03 Feb 2014 21:22:10 +0000
